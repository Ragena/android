package com.example.android.cameratest;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.imgproc.Imgproc;
import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import static org.opencv.core.CvType.CV_8U;
import static org.opencv.imgproc.Imgproc.CHAIN_APPROX_SIMPLE;
import static org.opencv.imgproc.Imgproc.INTER_NEAREST;
import static org.opencv.imgproc.Imgproc.RETR_EXTERNAL;
import static org.opencv.imgproc.Imgproc.THRESH_BINARY_INV;
import static org.opencv.imgproc.Imgproc.boundingRect;
import static org.opencv.imgproc.Imgproc.contourArea;
import static org.opencv.imgproc.Imgproc.findContours;
import static org.opencv.imgproc.Imgproc.rectangle;
import static org.opencv.imgproc.Imgproc.resize;
import static org.opencv.imgproc.Imgproc.threshold;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnTouchListener;
import android.view.SurfaceView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements OnTouchListener, CvCameraViewListener2{
    private static final String  TAG              = "OCVSample::Activity";
    private static final String MODEL_FILE        = "file:///android_asset/alphabet_softmax.pb";

    private boolean              mIsColorSelected = false;
    private Mat                  mRgba;
    private Mat                  mGray;
    //private Scalar               mBlobColorRgba;
    //private Scalar               mBlobColorHsv;
    //private ColorBlobDetector    mDetector;
    //private Mat                  mSpectrum;
    //private Size                 SPECTRUM_SIZE;
    //private Scalar               CONTOUR_COLOR;

    private CameraBridgeViewBase mOpenCvCameraView;

    private TensorFlowInferenceInterface inferenceInterface;

    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(MainActivity.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    public MainActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
        Contourizer.activity = this;
    }

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.color_blob_detection_activity_surface_view);
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);

        inferenceInterface = new TensorFlowInferenceInterface(getAssets(), MODEL_FILE);

    }

    @Override
    public void onPause()
    {
        Log.d (TAG, "onPause");
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        Log.d (TAG, "onResume");
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        Log.d (TAG, "onDestroy");
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    public void onCameraViewStarted(int width, int height) {
        Log.d (TAG, "onCameraViewStarted");
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        //mDetector = new ColorBlobDetector();
        //mSpectrum = new Mat();
        //mBlobColorRgba = new Scalar(255);
        //mBlobColorHsv = new Scalar(255);
        //SPECTRUM_SIZE = new Size(200, 64);
        //CONTOUR_COLOR = new Scalar(255,0,0,255);
    }

    public void onCameraViewStopped() {
        Log.d (TAG, "onCameraViewStopped");
        mRgba.release();
    }


    public boolean onTouch(View v, MotionEvent event) {
        Log.d (TAG, "onTouch");
        mIsColorSelected = true;
        return false;
    }

    private static final int SMALLEST_SIZE = 10;
    private static final int LARGEST_SIZE = 500;
    private static final int DST_SIZE = 50;

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        //Log.d (TAG, "onCameraFrame");
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

        //threshold(mGray, mGray, 130, 255, THRESH_BINARY_INV);

        List<MatOfPoint> contours = Contourizer.contourizer(mGray); //new ArrayList<MatOfPoint>();
        //findContours(mGray, contours, new Mat(), RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

        //List<MatOfPoint> characters = new ArrayList<MatOfPoint>();
        Iterator<MatOfPoint> it = contours.iterator();
        while(it.hasNext()) {
            MatOfPoint contour = it.next();
            //if (contourArea(contour) > SMALLEST_SIZE) {// && contourArea(contour) < LARGEST_SIZE) {
                //characters.add(contour);

                Rect b_rect = boundingRect(contour);
                rectangle(mRgba, b_rect.tl(), b_rect.br(), new Scalar(255, 0, 0, 128));

            //}
        }

        if (mIsColorSelected) {
            new Contourizer().execute(mGray);
            mIsColorSelected = false;
        }
        //Contourizer.getPaddedChars(mGray, contours);

        //Log.d (TAG, "onCameraFrame Done");
        return mRgba;
    }

    private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
        Log.d (TAG, "converScalarHsv2Rgba");
        Mat pointMatRgba = new Mat();
        Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
        Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);

        return new Scalar(pointMatRgba.get(0, 0));
    }

    private static final long[] IMAGE_SIZE = {1, 1600};

    public float[] runTF(List<Mat> characters) {
        float[] tRes = {0, 0, 0, 0, 0, 0};

        for (Mat cha : characters) {
            Mat ch = new Mat();
            ch = cha.reshape(1, 1);
            Log.d(TAG, String.format("type: %d", CV_8U));
            byte[] nCh = new byte[(int) ch.size().area()];
            ch.get(0, 0, nCh);

            float[] bCh = new float[(int) ch.size().area()];
            for (int i = 0; i < ch.size().area(); i ++) {
                bCh[i] = (nCh[i] != 0 )? 255: 0;
                //Log.i(TAG, String.format("byte %f", bCh[i]));
            }


            //ch.con

            inferenceInterface.feed("input", bCh, IMAGE_SIZE);
            inferenceInterface.run(new String[] {"output"});

            float[] res = {0, 0, 0, 0, 0, 0};
            inferenceInterface.fetch("output", res);
            //Log.i(TAG, String.format("Small Result: %f, %f, %f, %f, %f, %f",res[0],res[1],res[2],res[3],res[4],res[5]));
            for (int i = 0; i < 6; i++) {
                tRes[i] += res[i];
            }


        }

        Log.i(TAG, String.format("Result: %f, %f, %f, %f, %f, %f",tRes[0],tRes[1],tRes[2],tRes[3],tRes[4],tRes[5]));
        //Toast.makeText(this, String.format("Result: %s",tRes.toString()), Toast.LENGTH_SHORT).show();

        return tRes;

    }

}
