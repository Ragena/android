package com.example.android.cameratest;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Range;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.opencv.core.Core.BORDER_CONSTANT;
import static org.opencv.core.Core.copyMakeBorder;
import static org.opencv.core.CvType.CV_32F;
import static org.opencv.core.CvType.CV_32S;
import static org.opencv.core.CvType.CV_64F;
import static org.opencv.imgproc.Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C;
import static org.opencv.imgproc.Imgproc.CHAIN_APPROX_SIMPLE;
import static org.opencv.imgproc.Imgproc.INTER_LINEAR;
import static org.opencv.imgproc.Imgproc.INTER_NEAREST;
import static org.opencv.imgproc.Imgproc.RETR_EXTERNAL;
import static org.opencv.imgproc.Imgproc.THRESH_BINARY_INV;
import static org.opencv.imgproc.Imgproc.adaptiveThreshold;
import static org.opencv.imgproc.Imgproc.boundingRect;
import static org.opencv.imgproc.Imgproc.contourArea;
import static org.opencv.imgproc.Imgproc.findContours;
import static org.opencv.imgproc.Imgproc.rectangle;
import static org.opencv.imgproc.Imgproc.resize;
import static org.opencv.imgproc.Imgproc.threshold;

/**
 * Created by undo on 5/21/17.
 */

public class Contourizer extends AsyncTask<Mat, Void, float[]> {
    private static String TAG = "Contourizer";

    private static final int SMALLEST_SIZE = 10;
    private static final int LARGEST_SIZE = 500;
    private static final int DST_SIZE = 40;

    public static MainActivity activity;

    //public static Mat mImg = new Mat();
    private static List<MatOfPoint> mCharacters;

    public static List<MatOfPoint> contourizer(Mat img) {
        Mat nImg = new Mat();//img.rows(), img.cols(), img.type());
        threshold(img, nImg, 128, 255, THRESH_BINARY_INV);
        //adaptiveThreshold(img, img, 130, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY_INV, 10, 0);

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        findContours(nImg, contours, new Mat(), RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

        mCharacters = new ArrayList<MatOfPoint>();
        for (MatOfPoint contour : contours) {
            if (contourArea(contour) > SMALLEST_SIZE) {// && contourArea(contour) < LARGEST_SIZE) {
                mCharacters.add(contour);
            }
        }
        Log.d(TAG, String.format("characters: %d", mCharacters.size()));
        return mCharacters;
    }

    public static List<Mat> getPaddedChars(Mat img, List<MatOfPoint> contours) {
        List<Mat> mPaddedCharacters = new ArrayList<Mat>();

        for (MatOfPoint contour : contours) {
            Rect b_rect = boundingRect(contour);

            Mat ch = img.submat(b_rect);

            int nHeight, nWidth;

            if (b_rect.height > b_rect.width) {
                nHeight = DST_SIZE;
                nWidth = b_rect.width * nHeight / b_rect.height;
            } else {
                nWidth = DST_SIZE;
                nHeight = b_rect.height * nWidth / b_rect.width;
            }
            //Log.d(TAG, String.format("resizes: %d, %d", nWidth, nHeight));
            Mat rCh = new Mat();
            resize(ch, rCh, new Size(nWidth, nHeight), nWidth, nHeight, INTER_NEAREST);

            Mat nCh = new Mat(DST_SIZE, DST_SIZE, CV_32S, new Scalar(0));

            int top, bot, left, right;

            top = (DST_SIZE - nHeight) / 2;
            bot = DST_SIZE - top - nHeight;
            left = (DST_SIZE - nWidth) / 2;
            right = DST_SIZE - left - nWidth;

            copyMakeBorder(rCh, nCh, top, bot, left, right, BORDER_CONSTANT, new Scalar(0));

            byte[][] derp = new byte[(int)nCh.size().height][(int)nCh.size().width];
            //Log.d(TAG, String.format("Padded: %s", nCh.toString()));

            mPaddedCharacters.add(nCh);

            /*
            Mat aux;
            int startRange, stopRange;
            if (nHeight == DST_SIZE) {
                startRange = (DST_SIZE - nWidth) / 2;
                stopRange = startRange + nWidth;
                aux = nCh.colRange(startRange, stopRange);
            }
            else {
                startRange = (DST_SIZE - nHeight) / 2;
                stopRange = startRange + nHeight;
                aux = nCh.rowRange(startRange, stopRange);
            }
            Log.d(TAG, String.format("Sizes: (%f, %f), (%f, %f), (%f, %f), (%f, %f)",
                    aux.size().height, aux.size().width,
                    nCh.size().height, nCh.size().width,
                    ch.size().height, ch.size().width,
                    rCh.size().height, rCh.size().width));
            ch.copyTo(aux);

            if (aux.isSubmatrix()) {
                Log.d(TAG, "Matrix successfully padded");
                mPaddedCharacters.add(nCh);
            }
            else {
                Log.d(TAG, "Matrix was not padded successfully.");
            }*/

        }
        Log.d(TAG, String.format("Characters: %d", mPaddedCharacters.size()));
        return mPaddedCharacters;
    }

    @Override
    protected float[] doInBackground(Mat... params) {
        Mat img = params[0];

        return activity.runTF(getPaddedChars(img, mCharacters));

        //return null;
    }

    @Override
    protected void onPostExecute(float[] floats) {
        float fMax = 0;
        int iMax = 0;
        for(int i = 0; i < 6; i++)
        {
            if (floats[i] > fMax) {
                fMax = floats[i];
                iMax = i;
            }
        }
        switch (iMax) {
            case 0:
                Toast.makeText(activity, "Hangul", Toast.LENGTH_LONG).show();
                break;
            case 1:
                Toast.makeText(activity, "Latin", Toast.LENGTH_LONG).show();
                break;
            case 2:
                Toast.makeText(activity, "Greek", Toast.LENGTH_LONG).show();
                break;
            case 3:
                Toast.makeText(activity, "Persian", Toast.LENGTH_LONG).show();
                break;
            case 4:
                Toast.makeText(activity, "Cyrillic", Toast.LENGTH_LONG).show();
                break;
            case 5:
                Toast.makeText(activity, "Hebrew", Toast.LENGTH_LONG).show();
                break;
            default:
                Toast.makeText(activity, "Unknown", Toast.LENGTH_LONG).show();
        }
        //super.onPostExecute(floats);
    }
}
