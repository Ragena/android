package com.example.android.derp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;


public class MainActivity extends AppCompatActivity {

    static {
        System.loadLibrary("tensorflow_interface");
    }

    private static final String MODEL_FILE = "file:///android_asset/mnist_model_graph.pb";
    private static final String INPUT_NODE = "I";
    private static final String OUTPUT_NODE = "O";

    private static final int[] INPUT_SIZE = {1,3};

    private TensorFlowInferenceInterface inferenceInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inferenceInterface = new TensorFlowInferenceInterface();
        inferenceInterface.initializeTensorFlow(getAssets(), MODEL_FILE);

        float[] inputFloats = {1, 2, 3};

        inferenceInterface.fillNodeFloat(INPUT_NODE, INPUT_SIZE, inputFloats);
        inferenceInterface.runInference(new String[] {OUTPUT_NODE});

        float[] res = {0, 0};
        inferenceInterface.readNodeFloat(OUTPUT_NODE, res);

        Toast.makeText(this, String.format("done: %f %f", res[0], res[1]), Toast.LENGTH_SHORT).show();
    }
}
